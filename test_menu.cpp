/* 
	test_menu.cpp
	Author: M00736336 
	Created: 06/04/2021
	Updated: 14/04/2021 
*/

/*
	Brief: Testing validation and user inputs.
*/

#include "catch.hpp"
#include "Book.h"
#include "program.cpp"
#include <iostream>
#include <sstream>

static std::streambuf* original_cin = std::cin.rdbuf();
static std::streambuf* original_cout = std::cout.rdbuf();
static std::istringstream test_input;
static std::ostringstream test_output;
AVLTree<std::string, Book> test_books;

// Modified version of the menu add function.
void addTest(AVLTree<std::string, Book>& books, std::string path, 
	std::string title,std::string author,std::string ISBN,int qttyAvailable) {
	
	std::ifstream mainFile(path);
	loadFile(test_books, mainFile);
	
	
		if(title.length() == 0 || title == " " || title == "\t"){
        		std::cout << "\nString is empty, enter title again!";
		}
		if(author.length() == 0 || author == " " || author == "\t"){
			std::cout << "\nString is empty, enter author again!";
		}
		if(ISBN.length() == 0 || ISBN == " " || ISBN== "\t"){
 			std::cout << "\nString is empty, enter ISBN again!";
		}
 		if(qttyAvailable <= 0){
			std::cout << "\nQuantity has to be more than 0. Try Again!\n";
		}
	std::cout << "\nData Saved Succesfully\n";
	Book to_add = Book(title, author, ISBN, qttyAvailable);
	books.insert(title, to_add);
}

// Loads File.
TEST_CASE("Loading Books", "[Books]") {
	std::string path = "diffPath.txt";
	std::ifstream fileRead(path);
	fileRead.open(path);	
	
	REQUIRE_NOTHROW(loadFile(test_books, fileRead));
}

// Checks to see that the menu works.
TEST_CASE("Menu", "[menu]"){

	test_input.str("0\r\n");

	test_input.clear();

	std::cin.rdbuf(test_input.rdbuf());
	std::cout.rdbuf(test_output.rdbuf());

	REQUIRE_NOTHROW(menu());
	REQUIRE_THAT(
		test_output.str(),
		Catch::EndsWith(
			"Quitting Menu\n"));
}

// Adds a new book into the file.
TEST_CASE("Adding a New Book", "[Book]"){
  
	std::string path = "diffPath.txt";
	std::string title = "New Book";
	std::string author = "New Author";
	std::string ISBN = "97800202";
	int qttyAvailable = 5;

	REQUIRE_NOTHROW(addTest(test_books, path, title, author, ISBN, 
			qttyAvailable));
}

// Adds a book with invalid input in all sections.
TEST_CASE("Adding a New Book with invalid input", "[Book]"){
  
	std::string path = "diffPath.txt";
	std::string title = " ";
	std::string author = " ";
	std::string ISBN = " ";
	int qttyAvailable = 0;

	REQUIRE_NOTHROW(addTest(test_books, path, title, author, ISBN, 
			qttyAvailable));
}

// Checks to see if an existing book can be found
TEST_CASE("Searching Books", "[Books]"){
	std::string path = "diffPath.txt";
	std::ifstream fileRead(path);
	fileRead.open(path);	
	
	REQUIRE_NOTHROW(loadFile(test_books, fileRead));
	test_input.str("SQL\r\n"); 

	test_input.clear();

	std::cin.rdbuf(test_input.rdbuf());
	std::cout.rdbuf(test_output.rdbuf());

	REQUIRE_NOTHROW(searchBook(test_books));
}

// Checks to see if a non-existing book can be found
TEST_CASE("Searching Non-existing Books", "[Books]"){

	test_input.str("Random Book\r\n"); 

	test_input.clear();

	std::cin.rdbuf(test_input.rdbuf());
	std::cout.rdbuf(test_output.rdbuf());
	REQUIRE_NOTHROW(searchBook(test_books));
	REQUIRE_THAT(
		test_output.str(),
		Catch::EndsWith(
			"Couldn't find that book.\n"));
}

// Deletes an existing book
TEST_CASE("Deleting an Existing Book", "[Books]"){ 
	std::string path = "diffPath.txt";
 	test_input.str("SQL\r\n");

	test_input.clear();

	std::cin.rdbuf(test_input.rdbuf());
	std::cout.rdbuf(test_output.rdbuf());

	REQUIRE_NOTHROW(deleteBook(test_books, path));

}

// Checks to see if it can delete a non-existing book
TEST_CASE("Deleting Non-existing Book", "[Books]"){
	std::string path = "diffPath.txt";

 	test_input.str("NoBook\r\n"); 

	test_input.clear();

	std::cin.rdbuf(test_input.rdbuf());
	std::cout.rdbuf(test_output.rdbuf());

	REQUIRE_NOTHROW(deleteBook(test_books, path));
	REQUIRE_THAT(
		test_output.str(),
		Catch::EndsWith(
			"Couldn't find that book.\n"));
}



