CXX=g++
CFLAGS=-Wall

# This is a makefile. Author: M00736336

run: all
	./main

all: main.cpp
	$(CXX) main.cpp $(CFLAGS) -o main

test: test_main.o
	$(CXX) test_main.o test_menu.cpp $(CFLAGS) -o test_menu
	./test_menu

clean:
	rm -f *.o test_menu

.PHONY: all run clean test