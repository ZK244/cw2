#pragma once
#include <algorithm>
#include <iostream>
#include <vector>

/*
	bst-avl.h
	Author: M00736336
	Created: 22/03/2021
	Last Updated: 14/04/2021
*/

/*
	Self-Balancing Binary Search Tree, AVL tree.
*/

template <typename K, typename V>
class AVLTree {	
	struct Node {
        	K key;
		V value;
        	Node* left;
        	Node* right;
        	int height;

	Node(K key, V value) {
            this->key = key;
            this->value = value;
            this->left = nullptr;
            this->right = nullptr;
            this->height = 0;
        }
    };

	Node* root;

/*
	Function inserts nodes into the tree using left and right rotate.
	It checks the height difference between left and right subtrees
	to ensure that the height difference is not more than 1. 
*/
	Node* insert(Node* node, K key, V value) {
		if (node == nullptr) {
			node = new Node(key, value);
		} else if (key < node->key) {
			node->left = insert(node->left, key, value);
			if (height(node->left) - height(node->right) == 2) {
				if (key < node->left->key) {
					node = right_rotate(node);
				} else {
					node->left = left_rotate(node->left);
					node = right_rotate(node);
				}
			}
		} else if (key > node->key) {
			node->right = insert(node->right, key, value);
			if (height(node->right) - height(node->left) == 2) {
   				if (key > node->right->key) {
					node = left_rotate(node);
				} else {
					node->right = right_rotate(node->right);
					node = left_rotate(node);
				}
			}
		}
		node->height = std::max(height(node->left), height(node->right)) + 1;
		return node;
	}

/*
	Right and Left Rotate functions are both used for single rotations
	to keep the tree balanced. 
*/    
	Node* right_rotate(Node*& node) {
		Node* n = node->left;
		node->left = n->right;
		n->right = node;
		node->height = std::max(height(node->left), height(node->right)) + 1;
		n->height = std::max(height(n->left), node->height) + 1;
		return n;
	}

	Node* left_rotate(Node*& node) {
		Node* n = node->right;
		node->right = n->left;
		n->left = node;
		node->height = std::max(height(node->left), height(node->right)) + 1;
		n->height = std::max(height(node->right), node->height) + 1;
		return n;
	}

	int height(Node* node) { return (node == nullptr ? -1 : node->height); }

/*
	Search function scans through the tree using binary search to find the
	exact book that the user has inputted. 
*/ 

	Node* search(Node* node, K key) {
		if (node == nullptr) return node;

		if (key < node->key) {
			return search(node->left, key);
		} else if (key > node->key) {
			return search(node->right, key);
		} else {
			return node;
		}
	}

//Used in the delete function to find the minimum value (leftmost leaf).

	Node * minValueNode(Node* node) {
		Node* current = node; 
  		while (current->left != NULL) 
		current = current->left; 
		return current; 
	}

/*
	Delete function will find the exact book the user is searching for and delete
	the node from the tree. The tree will check the amount of children the node
	has and adjust accordingly.  
*/ 
	Node* deleteNode(Node* node, K key) { 
		if (node == nullptr) return node;
		else if (key < node->key) node->left = deleteNode(node->left, key);
		else if (key > node->key) node->right = deleteNode(node->right, key);
		else {
			// Node has no child (leaf node).
			if (node->left == nullptr && node->right == nullptr) {
				delete node;
				node = nullptr;
			}
			// Node has one child.
			else if (node->left == nullptr) {
				Node* temp = node;
				node = node->right;
				delete temp;
		  	}
			else if (node->right == nullptr) {
				Node* temp = node;
				node = node->left;
				delete temp;
			}
			// Node has two children.
			else {
				Node* temp = minValueNode(node->right);
				node->key = temp->key;
				node->right = deleteNode(node->right, temp->key);
			}
		  } 

		return node;
	}


	void cleanup(Node* node) {
		if (node == nullptr) return;
		cleanup(node->left);
		cleanup(node->right);
		delete node;
	}

	void recursive_keys(std::vector<K>& keys_list, Node* node) {
		if (node == nullptr) return;
	
		if (node->left != nullptr) {
			recursive_keys(keys_list, node->left);
		}
		keys_list.push_back(node->key);
		if (node->right != nullptr) {
			recursive_keys(keys_list, node->right);
		}
	}

	public:
		AVLTree() { root = nullptr; }
		~AVLTree() { cleanup(root); }
		void insert(K key, V value) { root = insert(root, key, value); }
		void remove(K key) { root = deleteNode(root, key); }
		bool contains(K key) { return search(root, key) != nullptr; }

	V& get(K key) {
		Node* result = search(root, key);

		if (result == nullptr) {
			throw std::runtime_error(
			"Lookup for key that does not exist in table.");
 		}
		return result->value;
	}

// The [] operator allows the tree[key] lookup. 
	V& operator[](K key) {
        // For first lookup. 
		if (root == nullptr) {
			root = new Node(key, V());
			return root->value;
        	}

        	Node* result = search(root, key);

        	// Lookup on non-existing key
        	if (result == nullptr) {
            		insert(key, V());
            		return get(key);
		}

        	// Lookup on existing key
		return result->value;
	}

	std::vector<K> keys() {
		std::vector<K> result;
		recursive_keys(result, root);
		return result;
	}
};
