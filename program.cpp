#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include "Book.h"
#include "bst-avl.h"

/*
	program.cpp
	Author: M00736336
	Created: 22/03/2021
	Last Updated: 14/04/2021
*/

/*
	Brief: File with all menu functions.
*/


// Template to check input specifications. 
template <class T>
void input(std::string const& prompt, T& value) {
	std::string str;
	std::cout << prompt;
	std::getline(std::cin, str);
	std::stringstream vstream(str);
	while (!(vstream >> value && (vstream >> std::ws).eof())) {
		std::cout << "Invalid input! Please try again.\n\n";
		std::cout << prompt;
		std::getline(std::cin, str);
		vstream.str(str);
		vstream.clear();
	}
}

// Template specialization for not validating strings.
template <>
void input(std::string const& prompt, std::string& value) {
	std::cout << prompt;
	std::getline(std::cin, value);
}

std::ostream& operator<<(std::ostream& os, Book& book) {
	os << book.to_string();
	return os;
}

std::string capitalLetter(std::string text) { 
	for (auto& c: text) c = toupper(c);
	return text;
}

// Saves items back into .txt file, and removes items with quantity 0.
void saveItems(AVLTree<std::string, Book>& books, std::string path){ 
	std::ofstream allData(path);
	for (const std::string& key : books.keys()) {
		books.get(key).setTitle(capitalLetter(key));
	}

	for (const std::string& key : books.keys()) {

    	if (books.get(key).getQttyAvailable()==0){ 
			books.remove(key);
    	} else {
			allData << books.get(key) << std::endl;
		}
	}

	allData.close();
}

// Reads from user given file, and saves book items as nodes in the AVL tree.
void loadFile(AVLTree<std::string, Book>& books, std::ifstream& fileRead) {
    std::string line, word;
        
	while (std::getline(fileRead, line)) {
		std::vector<std::string> split_line;
		std::istringstream line_stream(line);

		while (std::getline(line_stream, word, '\t')) {
			split_line.push_back(word);
		}
		
		books.insert(split_line[0], Book(split_line[0],
			split_line[1], split_line[2],std::stoi(split_line[3])
		));
	}
 
    fileRead.close();
}


// Adds books into the AVL tree and .txt file, while validating user input.
void addBook(AVLTree<std::string, Book>& books, std::string path) {
	std::string title;
	std::string author;
	std::string ISBN;
	int qttyAvailable;

	std::cout << "\nPlease enter the following information:\n";
	input("Item Title: ", title);
	while (title.length() == 0 || title == " " || title == "\t"){
		input("String is empty, enter title again: ", title);
	}
	for (size_t x = 0; x < title.length(); x++)	{
      		title[x] = toupper(title[x]);
    	}
    	while (books.contains(title)) {
      		input("This books already exists, try again: ", title);
      		while (title.length() == 0 || title == "\t"){
			input("String is empty, enter again: ", title);
		}
		for (size_t x = 0; x < title.length(); x++)	{
			title[x] = toupper(title[x]);
		}
	  }
	input("Author: ", author);
	while (author.length() == 0 || author == " " || author == "\t"){
		input("String is empty, enter author again: ", author);
	}
	input("ISBN: ", ISBN);
	while (ISBN.length() == 0 || ISBN == " " || ISBN== "\t"){
 		input("String is empty, enter ISBN again: ", ISBN);
	}
	input("Available Quantity: ", qttyAvailable);
 	while (qttyAvailable <= 0){
		std::cout << "Quantity has to be more than 0. Try Again!\n";
		input("Available Quantity: ", qttyAvailable);
	}
	for (size_t x = 0; x < author.length(); x++){
		if (x == 0){
			author[x] = toupper(author[x]);
		}else if (author[x-1] == ' '){
			author[x] = toupper(author[x]);
		}
	}

	Book to_add = Book(title, author, ISBN, qttyAvailable);
	books.insert(title, to_add);

	std::cout << "\nAdding book: " << to_add << std::endl;
	saveItems(books, path);
}

// Uses AVL functions to search through the tree and find the book.
void searchBook(AVLTree<std::string, Book>& books){
	std::string search_string;
	input("Please enter book title: ", search_string);

	for (size_t x = 0; x < search_string.length(); x++)	{
		search_string[x] = toupper(search_string[x]);
	}
  
	std::cout << "\nLooking for: "<< search_string << std::endl;			
	
	if (books.contains(search_string)) {
		std::cout << "\nFound the book you're looking for:\n" 
			  << books[search_string] << std::endl;
	} else {
		std::cout << "Couldn't find that book." << std::endl;
	}

}

// If the book exists, the node gets deleted from the AVL tree. 
void deleteBook(AVLTree<std::string, Book>& books, std::string path){
	std::string search_title;
	input("Please enter the title of the book you want to delete: ",search_title);

	for (size_t x = 0; x < search_title.length(); x++)	{
		search_title[x] = toupper(search_title[x]);
	}

	if (books.contains(search_title)) {  
    	books.remove(search_title);
		std::cout << "Book Deleted from System." << std::endl;
	} else {
		std::cout << "Couldn't find that book." << std::endl;
	}
	saveItems(books, path);
}

// Menu is called by main.cpp to start the command line program. 
void menu() {
	AVLTree<std::string, Book> books;
	int option;
	bool run = true; 
	std::string path = "books.txt";

	// Loads the provided file by default.
	std::ifstream mainFile(path);
	loadFile(books, mainFile);
	saveItems(books, path);

	while (run != false) {

		input(R"(
*********************************
1. Open file
2. Search for a book
3. Add a book
4. Remove a book
0. Quit
*********************************

)", option);

		std::cout << "You chose: " << option << "\n" << std::endl;
		
		switch (option) {
			case 1: {
				input("Please enter the path of your file: ", path);
        			std::ifstream fileRead(path);
				while (!fileRead){
    				input("Error! Please enter the path of your file: ", path);
		  			fileRead.open(path);
    			}
				loadFile(books, fileRead);
				break;
			} case 2: {
				searchBook(books);
				break;
			} case 3: {
 				addBook(books,path);
 				break;
			} case 4: {
				deleteBook(books, path);
				break;
			} case 0: {
				std::cout << "Quitting Menu" << std::endl;
				run = false;
				break;
			} default: {
  				std::cout << "Invalid option! Try again.\n" << std::endl;
				menu();
				break;
			}
		}
	}
}
