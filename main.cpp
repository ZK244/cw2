#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include "Book.h"
#include "bst-avl.h"
#include "program.cpp"

/*
	main.cpp
	Author: M00736336
	Created: 22/03/2021
	Last Updated: 14/04/2021
*/

/*
	Brief: Main class calls the menu() function to start the program.
*/


int main() {
	menu();
}