#pragma once
#include <sstream>

/*
	Book.h
	Author: M00736336
	Created: 21/03/2021
	Last Updated: 14/04/2021
*/

/*
	Brief: This is the oject Book class. 
*/


class Book {
public:
	std::string title;
	std::string author;
	std::string ISBN; 
	int qttyAvailable;

	Book(){}
	
	Book(const std::string& title, const std::string& author, 
		const std::string& ISBN, int qttyAvailable):

	title(title), author(author), ISBN(ISBN),
	qttyAvailable(qttyAvailable) {}
  

	std::string const getTitle(){
		return title;
	}

	void setTitle(std::string title){
 		this->title = title;
	}

	std::string getAuthor(){
		return author;
	}

	void setAuthor(std::string author){
 		this->author = author;
	}

 	std::string getISBN(){
		return ISBN;
	}

	void setISBN(std::string ISBN){
		this->ISBN = ISBN;
	}
	
	int getQttyAvailable(){
 		return qttyAvailable;
	}

 	void setQttyAvailable(int qttyAvailable){
		this->qttyAvailable = qttyAvailable;
	}

	std::string to_string() {
		std::ostringstream output;
		output << title << '\t' << author << '\t' << ISBN << '\t' << qttyAvailable;
		return output.str();
	}
};
